/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import com.is2t1.app.models.Contact;
import com.is2t1.app.views.ContactFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Sistemas-03
 */
public class ContactController implements ActionListener, FocusListener {
ContactFrame ContactForm;
JFileChooser d;
Contact contact;

public ContactController(ContactFrame f){
        super();
        ContactForm = f;
        d = new JFileChooser();
        contact = new Contact();
    }

    public ContactFrame getContactForm() {
        return ContactForm;
    }

    public void setContactForm(ContactFrame ContactForm) {
        this.ContactForm = ContactForm;
    }

    public JFileChooser getD() {
        return d;
    }

    public void setD(JFileChooser d) {
        this.d = d;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

        public void actionPerformed(ActionEvent e) {
       
        
        switch (e.getActionCommand()){
            case "save":
                d.showSaveDialog(ContactForm); 
                contact = ContactForm.getContactData();
                writePerson(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(ContactForm);  
                contact = readContact(d.getSelectedFile());
                ContactForm.setContactData(contact);
                
                break;
            case "clear":
                ContactForm.clear();
                break;

        }
        }

    private void writePerson(File file) {
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getContact());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(ContactFrame.class.getName()).log(Level.SEVERE, null, ex);
        }    }
    private Contact readContact(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Contact)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(ContactForm, e.getMessage(),ContactForm.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private Contact[] readPersonList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Contact[] persons;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
            persons = (Contact[]) s.readObject();
        }
        return persons;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    @Override
//    public void actionPerformed(ActionEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public void focusGained(FocusEvent e) {
       
    }

    @Override
    public void focusLost(FocusEvent e) {
        
    }
    
}
